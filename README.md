# IDS721-Mini-Project-1

[![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-1/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-1/-/commits/main)

# My Portfolio Website

Welcome to the repository for my personal portfolio website, where I showcase my projects and skills in data engineering and cloud computing.

## Features

- Home page with an introduction to my professional background.
- Portfolio section detailing my projects.

## Cloning the Repository

To clone the repository and view the website locally, follow these steps:

1. Open your terminal and run the following command:

   ```bash
   git clone https://gitlab.oit.duke.edu/jx139/ids721-mini-project-1.git
   ```

2. Change into the project directory:

   ```bash
   cd my-portfolio-site
   ```

## Running the Website Locally

To run the website on your local machine:

1. Inside the project directory, start the Zola server:

   ```bash
   zola serve
   ```

2. Open your web browser and navigate to the url in your terminal. You should now see the website running locally.

## Here are some screenshots of the website

![Home Page](./pic/homepage.png)
![Portfolio Section](./pic/portfolio.png)
