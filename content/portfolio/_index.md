+++
title = "My Portfolio"
+++

Here are some of the projects I have worked on. Each project showcases different skills and learning experiences.

- **Project 1**: Description of Project 1
- **Project 2**: Description of Project 2

For more details on each project, click on the project titles.